# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "casper/version"

Gem::Specification.new do |s|
  s.name          = "casper"
  s.version       = Casper::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "sdalu@sdalu.com" ]
  s.homepage      = "https://gitlab.com/sdalu/casper"
  s.summary       = "Fetching data from various providers"

  s.add_dependency 'httpx'
  s.add_dependency 'nokogiri'
  s.add_dependency 'faraday'
  s.add_dependency 'faraday-cookie_jar'
  s.add_dependency 'faraday-gzip'
  s.add_dependency 'faraday-encoding'
  s.add_dependency 'faraday-retry'
  s.add_dependency 'faraday-follow_redirects'
  s.add_dependency 'oauth2'

  
  s.add_development_dependency "faraday-detailed_logger"
  s.add_development_dependency "irb"
  s.add_development_dependency "yard"
  s.add_development_dependency "rake"
  s.add_development_dependency "redcarpet"

  s.license       = 'Apache-2.0'

  s.files         = %w[ LICENSE Gemfile casper.gemspec ] + 
		     Dir['lib/**/*.rb'] 
end
