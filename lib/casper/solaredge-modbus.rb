require 'json'
require 'date'
require 'time'


# See:
# * https://github.com/WillCodeForCats/solaredge-modbus-multi/wiki/Configuration
# * https://github.com/nmakel/solaredge_modbus

module Casper::Fetcher
class SolarEdgeModbus
    extend Casper::Fetcher

    STATUS_MAP = [ "undefined", "off", "sleeping", "grid-monitoring",
                   "producing", "throttled", "shutting-down",
                   "fault", "standby" ]

    def initialize(bin_dir, host, port, logger: nil, retries: 1)
        @bin_dir = bin_dir
        @cmd     = [ 'solaredge-modbus', '--json', host, port.to_s ]
    end

    
    ### Provider #########################################################

    provider :solaredge_modbus


    ### Fetchers #########################################################

    #
    # { <integer:pdl> : [ { time     : <Time>,
    #                       duration : <integer>,
    #                       energy   : <integer>,
    #                     }, ... ]
    #  ... }
    #
    fetcher :status do 
        json = IO.popen(@cmd, :err => File::NULL) { |io| io.read }
        data = JSON.parse(json)

        data['status'] = STATUS_MAP[data['status']] ||
                         (raise Casper::ParserError, "unknown status index")
        data['meters'].transform_values! do |v|
            _scaling(v)
        end
        _scaling(data)
    rescue JSON::ParserError
        raise Casper::ParserError, json
    end      


    private

    KEY_PARSER = /\A (?:l(?<l>\d+n?)_)?
                     (?<way>export_|import_)?
                     (?<key>.*?)
                     (?:_q(?<q>\d)|_ll|_ln)?
                  \z/x

    def _scaling(data)
        scales = data.map {|k,v|
            if k.end_with?('_scale')
                [ k[0..-7], (v == -32768) ? nil : 10 ** v ]
            end
        }.compact.to_h

        data.reject {|k,v| k.end_with?('_scale') }.map {|k, v|
            sk = KEY_PARSER.match(k)[:key]
            if scales.key?(sk)
                case scale = scales[sk]
                when 0   then [k ,v ]
                when nil then nil
                else          [ k, (v * scale).to_f ]
                end
            else
                [ k, v ]
            end
        }.compact.to_h
    end
end
end
