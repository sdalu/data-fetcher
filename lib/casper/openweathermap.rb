require 'date'
require 'faraday'
require 'faraday/retry'

# Free API: 60 calls / minutes


module Casper::Fetcher
class OpenWeatherMap
    extend Casper::Fetcher
    
    API_BASE_URI  = 'https://api.openweathermap.org'

    PRECIPITATION_TYPES  = [ 'none', 'rain', 'snow', 'mixed', 'ice' ]
    WEATHER_CODE_MAPPING = {
        200 => 'thunderstorm with light rain',
        201 => 'thunderstorm with rain',
        202 => 'thunderstorm with heavy rain',
        210 => 'light thunderstorm',
        211 => 'thunderstorm',
        212 => 'heavy thunderstorm',
        221 => 'scattered thunderstorm',
        230 => 'thunderstorm with light drizzle',
        231 => 'thunderstorm with drizzle',
        232 => 'thunderstorm with heavy drizzle',
        300 => 'light drizzle',
        301 => 'drizzle',
        302 => 'heavy drizzle',
        310 => 'light drizzle',
        311 => 'drizzle',
        312 => 'heavy drizzle',
        313 => 'shower drizzle',
        314 => 'heavy shower drizzle',
        321 => 'shower drizzle',
        331 => 'freezing-drizzle',
        500 => 'light rain',
        501 => 'rain',
        502 => 'heavy rain',
        503 => 'heavy rain',
        504 => 'heavy rain',
        511 => 'freezing-rain',
        512 => 'light freezing-rain',
        513 => 'heavy freezing-rain',
        520 => 'light shower rain',
        521 => 'shower rain',
        522 => 'heavy shower rain',
        531 => 'scattered shower rain',
        600 => 'light snow',
        601 => 'snow',
        602 => 'heavy snow',
        611 => 'ice-pellets',
        612 => 'light shower ice-pellets',
        613 => 'shower ice-pellets',
        615 => 'ligth sleet',
        616 => 'sleet',
        620 => 'light shower snow',
        621 => 'shower snow',
        622 => 'heavy shower snow',
        701 => 'mist',
        711 => 'smoke',
        721 => 'haze',
        731 => 'whirls',
        741 => 'fog',
        751 => 'sand',
        761 => 'dust',
        762 => 'ash',
        771 => 'squall',
        781 => 'tordano',
        800 => 'clear',
        801 => 'few clouds',
        802 => 'scattered clouds',
        803 => 'broken clouds',
        804 => 'overcast clouds',
    }


        
    
    def initialize(logger: nil)
        @logger = logger
        @site   = Faraday.new(url: API_BASE_URI) do |f|
            f.response :json
            f.request  :url_encoded
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    end

    
    ### Provider #########################################################

    provider :openweathermap


    ### Authentication ###################################################

    authentication :key do |apikey|
        @apikey = apikey
    end

    
    ### Fetchers #########################################################
    
    fetcher :pollutant do | lat:, lon: |
        @site.get('/data/2.5/air_pollution', {
                      :lat     => lat,
                      :lon     => lon,
                      :appid   => @apikey
                  }).body.compact.dig('list', 0).then {|item|
            OpenWeatherMap.convert_pollutant(item)
        }
    end
    
    fetcher :pollutant, :history do | lat:, lon:, from:, to: nil |
        # Ensure we don't fetch forecast data by clamping to now
        to ||= from + 7 * 24 * 60 * 60
        to   = to.clamp(nil, Time.now)
        
        @site.get('/data/2.5/air_pollution/history', {
                      :start   => from.to_time.to_i,
                      :end     => to  .to_time.to_i,
                      :lat     => lat,
                      :lon     => lon,
                      :appid   => @apikey
                  }).body.compact.dig('list').map {|item|
            OpenWeatherMap.convert_pollutant(item)
        }
    end
    
    fetcher :weather do | lat:, lon: |
        json = @site.get("/data/3.0/onecall", {
                             :lat     => lat,
                             :lon     => lon,
                             :appid   => @apikey,
                             :units   => 'metric',
                             :lang    => 'en',
                             :exclude => 'minutely'
                         }).body.compact
        { :current => json.dig('current').then {|json|
                          OpenWeatherMap.convert_weather(json, :current) },
          :hourly  => json.dig('hourly').map   {|json|
                          OpenWeatherMap.convert_weather(json, :hourly ) },
          :daily   => json.dig('daily' ).map   {|json|
                          OpenWeatherMap.convert_weather(json, :daily  ) }
        }
    end

    
    private
    
    def self.convert_pollutant(json)
        time = json.dig('dt'        )
        val  = json.dig('components')
        { :'time'         => Time.at(time),
          :'PM10'         => val['pm10' ],
          :'PM25'         => val['pm2_5'],
          :'CO'           => val['co'   ],
          :'NO'           => val['no'   ],
          :'NO2'          => val['no2'  ],
          :'O3'           => val['o3'   ],
          :'SO2'          => val['so2'  ],
          :'NH3'          => val['nh3'  ],
        }.compact
    end
    
    def self.convert_weather(json, mode = :current)
        time  = json.dig('dt')
        lwe   = ->(r,s,t) { (r || 0) + (s || 0) * 10.0 }

        # Weather
        # - temperature / humidity /pressure
        temperature    = case v = json.dig('temp')
                         when Numeric then v
                         when Hash    then [ v['min'], v['max'] ]
                         else raise Casper::ParserError,
                                    'unsupported temperature value'
                         end
        humidity       = json.dig('humidity'        )
        pressure       = json.dig('pressure'        )
        # - UV
        uv_index       = json.dig('uvi'             )
        # - visibility
        visibility     = json.dig('visibility'      )
        # - cloud
        c_cover        = json.dig('clouds'          )
        # - wind
        w_speed        = json.dig('wind_speed'      )
        w_gust         = json.dig('wind_gust'       )
        w_direction    = json.dig('wind_deg'        )
        # - precipitation
        p_probability  = if mode != :current
                             if pop = json.dig('pop')
                                 (pop * 100).to_i
                             end
                         end
        p_type         = PRECIPITATION_TYPES[
                             case mode
                             when :daily
                                 [ json.dig('rain'),
                                   json.dig('snow') ]
                             when :current, :hourly
                                 [ json.dig('rain', '1h'),
                                   json.dig('snow', '1h') ]
                             end.then do |rain, snow|
                                 (rain.nil? ? 0 : 1) + (snow.nil? ? 0 : 2)
                             end
                         ]
        p_accumulation = case mode
                         when :current, :hourly
                             lwe.(json.dig('rain', '1h'),
                                  json.dig('snow', '1h'),
                                  temperature)
                         when :daily
                             lwe.(json.dig('rain'),
                                  json.dig('snow'),
                                  temperature)
                         end
        p_timeslot     = case mode
                         when :current, :hourly then      60 * 60
                         when :daily            then 24 * 60 * 60
                         end
        # - weather code
        w_str          =  if v = json.dig('weather', 0, 'id')
                               WEATHER_CODE_MAPPING.fetch(v) {
                                   raise Casper::ParserError,
                                         "unknown weather code (#{v})"
                               }
                           end
        
        { :'time'                          => Time.at(time),
          :'temperature'                   => temperature,
          :'humidity'                      => humidity,
          :'pressure'                      => pressure,
          :'uv-index'                      => uv_index,
          :'visibility'                    => visibility,
          :'cloud-cover'                   => c_cover,
          :'wind-speed'                    => w_speed,
          :'wind-gust'                     => w_gust,
          :'wind-direction'                => w_direction,
          :'precipitation-probability'     => p_probability,
          :'precipitation-type'            => p_type,
          :'precipitation-accumulation'    => p_accumulation,
          :'precipitation-timeslot'        => p_timeslot,
          :'weather'                       => w_str
        }.compact
    end

end
end
