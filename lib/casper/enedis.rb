require 'tempfile'
require 'json'
require 'date'
require 'time'

#
# * requires NPM module "linky"
# * current day is not available
#
module Casper::Fetcher
class Enedis
    extend Casper::Fetcher
        
    def initialize(npm_dir, logger: nil, retries: 1)
        @npm_dir = npm_dir
        @linky   = [ 'npx', 'linky' ]
        @retries = retries
    end

    
    ### Provider #########################################################

    provider :enedis


    ### Fetchers #########################################################

    #
    # { <integer:pdl> : [ { time     : <Time>,
    #                       duration : <integer>,
    #                       energy   : <integer>,
    #                     }, ... ]
    #  ... }
    #
    fetcher :loadcurve, :history do | from, to = (Date.today - 1), pdl: |
        _linky(from, to, pdl: pdl, type: :loadcurve)
    end
 
    #
    # { <integer:pdl> : [ { time     : <Time>,
    #                       duration : <integer>,
    #                       energy   : <integer>,
    #                     }, ... ]
    #  ... }
    #
    fetcher :daily, :history do | from, to = (Date.today - 1), pdl: |
        _linky(from, to, pdl: pdl, type: :daily)
    end

    private

    def _linky(from, to, pdl:, type:)
        # Sanity check
        unless [ :daily, :loadcurve ].include?(type)
            raise ArgumentError, 'type expected :daily or :loadcurve'
        end

        # The to date is not included, so do a +1 and clamp
        to = (to + 1).clamp(.. Date.today)

        # Enedis sanity check
        return {} if from >= to

        # Perform query
        retries ||= @retries
        file      = Tempfile.new("linky-#{type}")
        system(*@linky, type.to_s,
               '-u', pdl.to_s,
               '-s', from.strftime("%F"),
               '-e', to  .strftime("%F"),
               '-o', file.path,
               :chdir => @npm_dir, :out => File::NULL, :err => File::NULL)
        { pdl => JSON.parse(file.read).dig('data').map {|values|
              # daily is in W, loadcurve is in Wh
              time             = Time.parse(values['date'])
              duration, energy =
                        case type
                        when :daily     then [ 24*60*60, values['value']   ]
                        when :loadcurve then [ 30*60,    values['value']/2 ]
                        end
              { :time => time, :duration => duration, :energy => energy }
          }
        }
    rescue JSON::ParserError
        if retries.zero?
            file.rewind
            raise Casper::ParserError, file.read
        end
        retries -= 1
        retry
    ensure 
        file&.close 
        file&.unlink 
    end
        
    
end
end
