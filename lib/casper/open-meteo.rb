require 'date'
require 'uri'
require 'faraday'

module Casper::Fetcher
class OpenMeteo
    include Casper::Fetcher

    API_URI         = 'https://api.open-meteo.com'
    API_ARCHIVE_URI = 'https://archive-api.open-meteo.com'
    API_AQI_URI     = 'https://air-quality-api.open-meteo.com'

    FIELDS_POLLUTANT = %w[pm10 pm2_5 carbon_monoxide nitrogen_dioxide sulphur_dioxide ozone aerosol_optical_depth dust]
    FIELDS_ALLERGEN  = %w[alder_pollen birch_pollen grass_pollen mugwort_pollen olive_pollen ragweed_pollen]
    FIELDS_RADIATION = %w[shortwave_radiation direct_radiation diffuse_radiation direct_normal_irradiance terrestrial_radiation]
    FIELDS_WEATHER   = %w[temperature_2m relativehumidity_2m
                          precipitation rain showers snowfall
                          weathercode pressure_msl uv_index visibility
                          cloud_cover
                          windspeed_10m winddirection_10m windgusts_10m]


    FIELDS_ARCHIVE_UNSUPPORTED = [ 'showers', 'weathercode', 'terrestrial_radiation' ]
    
    # http://www.skystef.be/obs-weather.htm
    WEATHER_CODE_MAPPING = {
         0 => 'clear',
         1 => 'few clouds',
         2 => 'scattered clouds',
         3 => 'overcast clouds',
        45 => 'fog',
        48 => 'rime fog',
        51 => 'light drizzle',
        53 => 'drizzle',
        55 => 'heavy drizzle',
        56 => 'light freezing-drizzle',
        57 => 'heavy freezing-drizzle',
        61 => 'light rain',
        63 => 'rain',
        65 => 'heavy rain',
        66 => 'light freezing-rain',
        67 => 'heavy freezing-rain',
        71 => 'light snow',
        73 => 'snow',
        75 => 'heavy snow',
        77 => 'snow',
        80 => 'light shower rain',
        81 => 'shower rain',
        82 => 'heavy shower rain',
        85 => 'light shower snow',
        86 => 'heavy shower snow',
        95 => 'thunderstorm',
        96 => 'thunderstorm with hail',
        99 => 'heavy thunderstorm with hail',
    }

    
    def initialize(logger: nil)
        @logger = logger
        @api, @api_archive, @api_aqi = [
            API_URI, API_ARCHIVE_URI, API_AQI_URI ].map {|uri|
            Faraday.new(url: uri) do |f|
                f.response :json
                f.request  :url_encoded
                f.response :raise_error
                f.response :logger, @logger if @logger
            end
        }
    end

    
    ### Provider #########################################################

    provider :open_meteo


    ### Fetchers #########################################################

    fetcher :pollutant do | lat:, lon: |
        @api_air.get('/v1/air-quality', {
            :latitude   => lat,
            :longitude  => lon,
            :timeformat => 'unixtime',
            :past_days  => 7,
            :hourly     => FIELDS_POLLUTANT + FIELDS_ALLERGEN
        }).body.compact.then {|item|
            OpenMeteo.data_collapse(item).map {|w|
                OpenMeteo.convert_pollutant(w)
            }
        }
    end

    fetcher :weather do | lat:, lon:, past: nil |
        past = past&.clamp(0, 90) || 7
        
        @api.get('/v1/forecast', {
            :latitude           => lat,
            :longitude          => lon,
            :timeformat         => 'unixtime',
            :windspeed_unit     => 'ms',
            :temperature_unit   => 'celsius',
            :precipitation_unit => 'mm',
            :past_days          => past,
            :hourly             => FIELDS_WEATHER + FIELDS_RADIATION
        }).body.compact.then {|item|
            OpenMeteo.data_collapse(item).map {|w|
                OpenMeteo.convert_weather(w)
            }
        }
    end

    fetcher :weather, :history do | lat:, lon:, from:, to: nil |
        from = from&.to_date
        to   = to  &.to_date

        # Ensure we don't fetch date outside history range
        to ||= from.next_month
        to   = to.clamp(nil, Date.today - 7)

        
        @api_archive.get('/v1/era5', {
            :latitude           => lat,
            :longitude          => lon,
            :timeformat         => 'unixtime',
#           :timezone           => 'Europe/Paris',
            :windspeed_unit     => 'ms',
            :temperature_unit   => 'celsius',
            :precipitation_unit => 'mm',
            :start_date         => from.to_date,
            :end_date           => to  .to_date,
            :hourly             => FIELDS_WEATHER + FIELDS_RADIATION -
                                   FIELDS_ARCHIVE_UNSUPPORTED
        }).body.compact.then {|item|
            OpenMeteo.data_collapse(item).map {|w|
                OpenMeteo.convert_weather(w)
            }
        }
    end
    
    private
    
    def self.data_collapse(json)
        list = json.dig('hourly')
        keys = list.keys
        ( 0 ... list.map {|k, l| l.size }.max).map {|i|
            keys.to_h {|k| [k, list.dig(k, i) ] }.compact
        }
    end

    def self.convert_pollutant(json)
        time  = json.dig('time')

        # Pollutant
        p = { :'time'         => time,
              :'PM10'         => json.dig('pm10'                 ),
              :'PM25'         => json.dig('pm2_5'                ),
              :'CO'           => json.dig('carbon_monoxide'      ),
              :'NO2'          => json.dig('nitrogen_dioxide'     ),
              :'O3'           => json.dig('ozone'                ),
              :'SO2'          => json.dig('sulphur_dioxide'      ),
              :'dust'         => json.dig('dust'                 ),
              :'AOD'          => json.dig('aerosol_optical_depth'),
            }.compact
        p = nil if p.one?
        
        # Allergen
        a = { :'time'        => time,
              :'alder'       => json.dig('alder_pollen'          ),
              :'birch'       => json.dig('birch_pollen'          ),
              :'grass'       => json.dig('grass_pollen'          ),
              :'mugwort'     => json.dig('mugwort_pollen'        ),
              :'olive'       => json.dig('olive_pollen'          ),
              :'ragweed'     => json.dig('ragweed_pollen'        ),
            }.compact
        a = nil if a.one?
    end
    
    def self.convert_weather(json)
        time  = json.dig('time')

        # Weather
        # - temperature / humidity / pressure
        temperature    = json.dig('temperature_2m'     )
        humidity       = json.dig('relativehumidity_2m')
        pressure       = json.dig('pressure_msl'       )
        # - UV
        uv_index       = json.dig('uv_index'           )
        # - visibility
        visibility     = json.dig('visibility'         )
        # - cloud
        c_cover        = json.dig('cloud_cover'        )
        # - irradiance
        i_global_horizontal  = json.dig('shortwave_radiation')      # GHI
        i_diffuse_horizontal = json.dig('diffuse_radiation')        # DHI
        i_direct_horizontal  = json.dig('direct_radiation')
        i_direct_normal      = json.dig('direct_normal_irradiance') # DNI
        # - wind
        w_speed        = json.dig('windspeed_10m'      )
        w_gust         = json.dig('windgusts_10m'      )
        w_direction    = json.dig('winddirection_10m'  )
        # - precipitation
        p_type         = if json.dig('precipitation').nil?
                             nil
                         elsif json.dig('precipitation').zero?
                             'none'
                         else
                             rain = ! [ json.dig('rain'), json.dig('showers')
                                      ].compact.max.zero?
                             snow = ! json.dig('snowfall').zero?
                             if rain && snow then 'mixed'
                             elsif rain then 'rain'
                             elsif snow then 'snow'
                             else            'rain'
                             end
                         end                             
        p_accumulation = json.dig('precipitation')
        p_timeslot     = 60 * 60
        # - weather code
        w_str          =  if v = json.dig('weathercode')&.to_i
                               WEATHER_CODE_MAPPING.fetch(v) {
                                   raise Casper::ParserError,
                                         "unknown weather code (#{v})"
                               }
                           end
        
        { :'time'                          => Time.at(time),
          :'temperature'                   => temperature,
          :'humidity'                      => humidity,
          :'pressure'                      => pressure,
          :'uv-index'                      => uv_index,
          :'visibility'                    => visibility,
          :'cloud-cover'                   => c_cover,
          :'irradiance-global-horizontal'  => i_global_horizontal,
          :'irradiance-diffuse-horizontal' => i_diffuse_horizontal,
          :'irradiance-direct-horizontal'  => i_direct_horizontal,
          :'irradiance-direct-normal'      => i_direct_normal,
          :'wind-speed'                    => w_speed,
          :'wind-gust'                     => w_gust,
          :'wind-direction'                => w_direction,
          :'precipitation-type'            => p_type,
          :'precipitation-accumulation'    => p_accumulation,
          :'precipitation-timeslot'        => p_timeslot,
          :'weather'                       => w_str
        }.compact
    end
    
end
end
