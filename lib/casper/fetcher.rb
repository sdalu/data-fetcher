module Casper

class ResourceFetcher
    def initialize(&block)
        @block = block
    end

    def fetch(*args, **opts)
        @block.call(*args, **opts)
    end
end

module Fetcher
    def self.included(o)
        super
        o.include(FetcherInstanceMethods)
        o.extend(FetcherClassMethods)
        o.__fetcher_init
    end

    def self.extended(o)
        super
        o.include(FetcherInstanceMethods)
        o.extend(FetcherClassMethods)
        o.__fetcher_init
    end
end
    
module FetcherInstanceMethods
    def [](*path)
        description = self.class._fetcher(*path)
        me          = self
        
        ResourceFetcher.new do |*args, **opts|
            me.instance_exec(*args, **opts, &description[:action])
        rescue AuthError
            case self.class._authentication[:type]
            when :session, :token            # Re-authenticate and retry
                me.reauthenticate
                me.instance_exec(*args, **opts, &description[:action])
            else                             # Re-raise exception
                raise
            end            
        end
    end

    def reauthenticate
        if !defined?(@__fetcher_authentication_args)
            raise "authentication has not been performed before"
        end
        
        instance_exec(*@__fetcher_authentication_args,
                      &self.class._authentication[:action])
        self
    end
    
    def authenticate?
        self.class._authentication[:type] != :none
    end
    
    def authenticate(*args)
        @__fetcher_authentication_args = args

        description = self.class._authentication

        if description[:type] == :none
            raise "no authentication required"
        end
        
        instance_exec(*args, &description[:action])
        self
    rescue Faraday::ResourceNotFound
        raise Casper::NetError
    end
end

module FetcherClassMethods
    def provider(name)
        Casper.register(name, self)
    end

    def __fetcher_init
        @__fetcher_paths          = {}
        @__fetcher_authentication = { :type => :none }
    end
    
    def fetcher(*path, &block)
        description = { :path   => path,
                        :action => block
                      }

        @__fetcher_paths.merge!(path => description) {
            raise ArgumentError,
                  "fetcher :#{path.join('/')} for #{self} already defined"
        }
    end

    def authentication(type, &block)
        case type
        when :none
            @__fetcher_authentication.merge!(:type => type)
        when :key, :session, :token
            @__fetcher_authentication.merge!(:type => type, :action => block)
        when :import, :export
            @__fetcher_authentication.merge!(type => block)
        else raise ArgumentError
        end
    end

    def _fetcher(*path)
        @__fetcher_paths.fetch(path) {
            raise ArgumentError,
                  "unsupported resource :#{path.join('/')} for #{self}"
        }
    end

    def _authentication
        @__fetcher_authentication
    end
    
end

end
