require 'faraday'
require 'faraday/retry'

module Casper::Fetcher
class Tomorrow
    extend Casper::Fetcher
    
    API_URI  = 'https://api.tomorrow.io'

    PRECIPITATION_TYPES = [ 'none', 'rain', 'snow', 'mixed', 'ice' ]
    WEATHER_CODE_MAPPING = {
        1000 => 'clear',
        1100 => 'few clouds',
        1101 => 'scattered clouds',
        1102 => 'broken clouds',
        1001 => 'overcast clouds',
        2000 => 'fog',
        2100 => 'light fog',
        4000 => 'drizzle',
        4001 => 'rain',
        4200 => 'light rain',
        4201 => 'heavy rain',
        5000 => 'snow',
        5001 => 'light snow',
        5100 => 'light snow',
        5101 => 'heavy snow',
        6000 => 'freezing-drizzle',
        6001 => 'freezing-rain',
        6200 => 'light freezing-rain',
        6201 => 'heavy freezing-rain',
        7000 => 'ice-pellets',
        7101 => 'heavy ice-pellets',
        7102 => 'light ice-pellets',
        8000 => 'thunderstorm', 
    }
    
    
    FIELDS = [
        # Weather
        'temperature',
        'humidity',
        'windSpeed',
        'windDirection',
        'windGust',
        'pressureSurfaceLevel',
#       'pressureSeaLevel',
#       'rainIntensity',
#       'freezingRainIntensity',
#       'snowIntensity',
#       'sleetIntensity',
        'precipitationProbability',
        'precipitationType',
        'precipitationIntensity',
#       'rainAccumulation',
#       'snowAccumulationLwe',
#       'sleetAccumulationLwe',
#       'iceAccumulationLwe',
        'visibility',
        'cloudCover',
        'cloudBase',
        'cloudCeiling',
        'uvIndex',
#       'uvHealthConcern',
        'solarGHI',
        'solarDNI',
        'solarDHI',
        'lightningFlashRateDensity',
        'weatherCode',

        # Pollutant
        'particulateMatter25',
        'particulateMatter10',
        'pollutantO3',
        'pollutantNO2',
        'pollutantCO',
        'pollutantSO2',
        'mepIndex',
        'epaIndex',

        # Allergen
        'treeIndex',
        'grassIndex',
    ]
    
    
    def initialize(logger: nil)
        @logger = logger
        @site   = Faraday.new(url: API_URI) do |f|
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.request  :retry, :max => 2, :interval => 5,
                               :retry_statuses => [ 429 ]
            f.response :logger, @logger if @logger
        end
    end

    ### Provider #########################################################

    provider :tomorrow


    ### Authentication ###################################################

    authentication :key do |apikey|
        @apikey = apikey
    end


    ### Fetchers #########################################################

    #
    #  {  time                           : <Time>,
    #    ?duration                       : <integer>,
    #    ?temperature                    : <float>
    #    ?humidity                       : <float>,
    #    ?pressure                       : <float>,
    #    ?uv-index                       : <float>,
    #    ?visibility                     : <float>,
    #    ?cloud-cover                    : <float>,
    #    ?cloud-base                     : <float>,
    #    ?cloud-ceiling                  : <float>,
    #    ?irradiance-global-horizontal   : <float>,
    #    ?irradiance-diffuse-horizontal  : <float>,
    #    ?irradiance-direct-normal       : <float>,
    #    ?wind-speed                     : <float>,
    #    ?wind-gust                      : <float>,
    #    ?wind-direction                 : <float>,
    #    ?precipitation-probability      : <float>,
    #    ?precipitation-type             : <float>,
    #    ?precipitation-accumulation     : <float>,
    #    ?precipitation-timeslot         : <float>,
    #    ?lightning                      : <float>,
    #    ?weather                        : <float>
    #  }
    #
    # pollutant = {  time : <Time>,
    #               ?PM25 : <float>,
    #               ?PM10 : <float>,
    #               ?CO   : <float>,
    #               ?NO   : <float>,
    #               ?NO2  : <float>,
    #               ?O3   : <float>,
    #               ?SO2  : <float>,
    #               ?NH3  : <float>,
    #               ?C6H6 : <float>,
    #             }
    fetcher :environment do | lat:, lon: |
        json = @site.get('/v4/timelines', {
                             :location  => "#{lat},#{lon}",
                             :apikey    => @apikey,
                             :timesteps => "current",
                             :units     => 'metric',
                             :fields    => FIELDS.join(',')
                         }).body.compact

        _convert(json.dig('data', 'timelines', 0, 'intervals', 0), :current)
    end
    
    private
    
    def _convert(json, mode = :current)
        time  = Time.parse(json.dig('startTime'))
        vs    = json.dig('values')
        scale = ->(x, f) { x && (x * f) }
        
        # Weather
        # - temperature / humidity /pressure
        temperature    = vs['temperature'         ]
        humidity       = vs['humidity'            ]
        pressure       = vs['pressureSurfaceLevel']
        # - UV
        uv_index       = vs['uvIndex'             ]
        # - visibility
        visibility     = scale.(vs['visibility'   ], 1000)
        # - cloud
        c_cover        = vs['cloudCover'          ]
        c_base         = scale.(vs['cloudBase'    ], 1000)
        c_ceiling      = scale.(vs['cloudCeiling' ], 1000)
        # - irradiance
        i_global_horizontal  = vs['solarGHI'      ]
        i_diffuse_horizontal = vs['solarDHI'      ] 
        i_direct_normal      = vs['solarDNI'      ] 
        # - wind
        w_speed        = vs['windSpeed'           ]
        w_gust         = vs['windGust'            ]
        w_direction    = vs['windDirection'       ]
        # - precipitation
        p_probability  = if mode != :current
                             vs['precipitationProbability']
                         end
        p_type         = if v = vs['precipitationType']
                             PRECIPITATION_TYPES.fetch(v) {
                                 raise Casper::ParserError,
                                       'precipitation type mapping unavailable'
                             }
                         end
        p_accumulation = if mode == :current
                             vs['precipitationIntensity']
                         else
                             (vs['rainAccumulation'    ] || 0) +
                             (vs['snowAccumulationLwe' ] || 0) +
                             (vs['sleetAccumulationLwe'] || 0) +
                             (vs['iceAccumulationLwe'  ] || 0)
                         end
        p_timeslot     = case mode
                         when :current, :hourly then      60 * 60
                         when :daily            then 24 * 60 * 60
                         end
        # - lightning
        l_frd          = scale.(vs['lightningFlashRateDensity'], 0.2)
        # - weather code
        w_str          =  if v = vs['weatherCode']
                               WEATHER_CODE_MAPPING.fetch(v) {
                                   raise Casper::ParserError,
                                         "unknown weather code (#{v})"
                               }
                           end
        
        w = { :'time'                          => time,
              :'temperature'                   => temperature,
              :'humidity'                      => humidity,
              :'pressure'                      => pressure,
              :'uv-index'                      => uv_index,
              :'visibility'                    => visibility,
              :'cloud-cover'                   => c_cover,
              :'cloud-base'                    => c_base,
              :'cloud-ceiling'                 => c_ceiling,
              :'irradiance-global-horizontal'  => i_global_horizontal,
              :'irradiance-diffuse-horizontal' => i_diffuse_horizontal,
              :'irradiance-direct-normal'      => i_direct_normal,
              :'wind-speed'                    => w_speed,
              :'wind-gust'                     => w_gust,
              :'wind-direction'                => w_direction,
              :'precipitation-probability'     => p_probability,
              :'precipitation-type'            => p_type,
              :'precipitation-accumulation'    => p_accumulation,
              :'precipitation-timeslot'        => p_timeslot,
              :'lightning'                     => l_frd,
              :'weather'                       => w_str
            }.compact
        w = nil if w.one?
        
        # Pollutant
        p = { :'time'         => time,
              :'PM10'         => vs['particulateMatter10'],
              :'PM25'         => vs['particulateMatter25'],
              :'CO'           => scale.(vs['pollutantCO' ], 1.145),
              :'NO2'          => scale.(vs['pollutantNO2'], 1.88 ),
              :'O3'           => scale.(vs['pollutantO3' ], 2.00 ),
              :'SO2'          => scale.(vs['pollutantSO2'], 2.62 ),
              :'EPA-index'    => vs['epaIndex'           ],
              :'MEP-index'    => vs['mepIndex'           ],
            }.compact
        p = nil if p.one?
        
        # Allergen
        a = { :'time'        => time,
              :'tree-index'  => vs['treeIndex'          ],
              :'grass-index' => vs['grassIndex'         ],
            }.compact
        a = nil if a.one?

        # Weather / Pollutant / Allergen
        [ w, p, a ]
    end
    

end
end
