require 'date'
require 'csv'
require 'faraday'
require 'faraday/follow_redirects'

# https://www.data.gouv.fr/en/datasets/donnees-temps-reel-de-mesure-des-concentrations-de-polluants-atmospheriques-reglementes-1/


module Casper::Fetcher
class DataGouv
    extend Casper::Fetcher
    
    SITE_BASE_URI    = 'https://www.data.gouv.fr'
    FILES_BASE_URI   = 'https://files.data.gouv.fr'

    LCSQA_HEADER     = [
        "﻿Date de début", "Date de fin", "Organisme", "code zas", "Zas",
        "code site", "nom site", "type d'implantation", "Polluant",
        "type d'influence", "discriminant", "Réglementaire",
        "type d'évaluation", "procédure de mesure", "type de valeur", "valeur",
        "valeur brute", "unité de mesure", "taux de saisie",
        "couverture temporelle", "couverture de données", "code qualité",
        "validité"
    ]
    LCSQA_POLLUTANTS = {
        "PM10"   => :PM10,   "PM2.5"  => :PM25,
        "SO2"    => :SO2,
        "O3"     => :O3,
        "NO"     => :NO,     "NO2"    => :NO2,   "NOX as NO2" => :NOX,
        "C6H6"   => :C6H6,
        "CO"     => :CO
    }
                           

    def initialize(logger: nil, retries: nil)
        @logger = logger
        @site   = Faraday.new(url: SITE_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :raise_error
            f.response :follow_redirects
            f.response :logger, @logger if @logger
        end
        @files  = Faraday.new(url: FILES_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    end

    
    ### Provider #########################################################

    provider  :datagouv


    ### Fetchers #########################################################
    
    #
    # {  time : <Time>,
    #   ?PM25 : <float>,
    #   ?PM10 : <float>,
    #   ?CO   : <float>,
    #   ?NO   : <float>,
    #   ?NO2  : <float>,
    #   ?O3   : <float>,
    #   ?SO2  : <float>,
    #   ?NH3  : <float>,
    #   ?C6H6 : <float>,
    #  }
    #
    fetcher :pollutant, :history do |date, zas: nil, site: nil|
        code_zas  = zas&.upcase
        code_site = site&.upcase

        file = "FR_E2_" + date.strftime("%Y-%m-%d") + ".csv"
        year = date.year
        data = @files.get("/lcsqa/concentrations-de-polluants-atmospheriques-reglementes/temps-reel/#{year}/#{file}").body
        data.force_encoding('UTF-8')

        csv = CSV.parse(data, :headers => true, :col_sep => ';')

        if csv.headers != LCSQA_HEADER
            raise Casper::ParserError, 'data or data order has changed'
        end

        data = {}
        csv.each_with_index do |row, idx| row = row.to_h
            # Retrieve fields
            site      = row["code site"].upcase
            zas       = row["code zas" ].upcase
            key       = [ site, zas ].join(':')
            time      = Time.parse(row["﻿Date de début"])
            pollutant = LCSQA_POLLUTANTS.fetch(row["Polluant"]) { |k|
                raise Casper::ParserError, "unhandled pollutant (#{k})" }
            value     = row["valeur"].then {|v| v.nil? ? nil : Float(v) }
            unit      = row["unité de mesure"]
            validated = Integer(row["validité"]) == 1
            entry     = '<'+[site, time.strftime("%F %T"), idx+1].join('#')+'>'

            # No information?
            next if pollutant.nil?
            next if value.nil?

            # Unwanted data?
            next if code_zas  && code_zas  != zas
            next if code_site && code_site != site

            # Unit convertion
            case unit
            when "mg-m3"          then value *= 1000
            when "µg-m3", "µg/m3" then # nothing
            else raise Casper::ParserError,
                       "unexpected unit #{unit} for #{pollutant} @#{entry}"
            end

            # Merging data
            data[key]       ||= {}
            data[key][time] ||= {}

            vals = data[key][time].merge!(pollutant => value) {|k, o, n|
                @logger&.warn "duplicate #{k} @#{entry}"
            }

            if ! validated
                warn "unvalidated #{pollutant} @#{entry}"
                vals.merge!(:unvalidated => [pollutant]) {|k,o,n| o + n }
            end
        end

        # Compact and merge time
        data.transform_values! {|ts|
            ts.sort.map {|time, values| values.compact.merge(:time => time) }
        }
    end

end
end
