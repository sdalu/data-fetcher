require 'date'
require 'uri'
require 'faraday'


module Casper::Fetcher
class Daikin
    include Casper::Fetcher
        
    def initialize(base_uri, logger: nil, retries: nil)
        @logger = logger
        @api    = Faraday.new(url: base_uri) do |f|
            f.request  :url_encoded
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    end

    
    ### Provider #########################################################

    provider :daikin


    ### Fetchers #########################################################

    #
    # { temperature_indoor  : <float>,
    #   temperature_outdoor : <float>,
    # }
    fetcher :sensors do
        data = _parse(@api.get('/aircon/get_sensor_info').body)
        { :temperature_indoor  => Float(data['htemp']),
          :temperature_outdoor => Float(data['otemp']),
        }
    end

    
    fetcher :info do 
        data = _parse(@api.get('/common/basic_info').body)
    end

    private

    def _parse(data)
        data.split(',')
            .map  {|c| URI.decode_www_form_component(c) }
            .to_h {|kv| kv.split('=', 2) }
    end
end
end
